﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DollarEffect : MonoBehaviour
{
    private Image m_image;
    private Vector3 m_Start;
    private Vector3 m_End;
    private float time = 1.0f;

    // Start is called before the first frame update
    void Awake()
    {
        m_image = GetComponent<Image>();
        m_Start = transform.localPosition;
        m_End = m_Start + Vector3.up * 100.0f;
    }

    // Update is called once per frame
    void Update()
    {
        // Update Move
        transform.localPosition = Vector3.Lerp(m_Start, m_End, time);
        // Update Alpha
        Color c = m_image.color;
        c.a = Mathf.Lerp(1.0f, 0.0f, 1.0f - (1.0f - time) * (1.0f - time));
        m_image.color = c;

        // Update Time
        time += (1.0f / 30.0f);

        time = Mathf.Clamp01(time);
    }

    public void Kaching()
    {
        time = 0.0f;
    }
}
