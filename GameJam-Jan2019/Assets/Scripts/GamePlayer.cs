﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class GamePlayer : MonoBehaviour
{
    // Private
    private GameCamera m_cameraReference;
    private CharacterController m_characterController;
    private PlayerIndex m_controller_index;
    private Vector2 m_analogMovement;
    private Vector3 m_playerMovement;
    private Vector3 m_nudge = new Vector3(0, 0, 0);
    private Vector3 m_nudgeFixed = new Vector3(0, 0, 0);
    // Start Position
    [System.NonSerialized]
    public Vector3 m_spawnPosition;

    private bool[] m_buttonAState = { false, false };
    private bool[] m_buttonXState = { false, false };


    private bool m_buttonInteract = false;
    private bool m_buttonAttack = false;

    // Public
    public float PlayerSpeed = 0.2f;
    [Range(0.001f, 0.9999f)]
    public float NudgeRemover = 0.95f;
    public float NudgeStrength = 2.0f;
    public float NudgeDistanceMin = 3.0f;
    public LayerMask PlayerCollision;

    private bool m_hurt = false;
    private float m_hurtTimeAmount = 6.0f;
    private float m_hurtTime = 0;

    public bool IsHurt()
    {
        return m_hurt;
    }
    public void Hurt()
    {
        m_hurt = true;
        m_hurtTime = m_hurtTimeAmount;
    }

    // Hold Item
    [System.NonSerialized]
    public GameObject m_holdItem = null;

    void Awake()
    {
        if (gameObject.CompareTag("Player1"))
        {
            m_controller_index = PlayerIndex.One;
            GameData.m_player1 = this;
        }
        else if (gameObject.CompareTag("Player2"))
        {
            m_controller_index = PlayerIndex.Two;
            GameData.m_player2 = this;
        }

        m_cameraReference = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<GameCamera>();
        if (m_cameraReference == null)
            Debug.LogError("PLAYER: CANNOT FIND CAMERA");

        m_characterController = gameObject.GetComponent<CharacterController>();
        if (m_characterController == null)
            Debug.LogError("PLAYER: MISSING CHARACTER CONTROLLER");

        // Raycast floor and fix Y value
        bool Fail = false;
        float floorY = RaycastSimple.Floor(transform.position, PlayerCollision, ref Fail).y;
        floorY += (m_characterController.height / 2) * transform.localScale.y;
        if (!Fail)
            transform.SetY(floorY);

        // Set Spawn Position
        m_spawnPosition = transform.position;
    }

    public void Nudge(Vector3 v)
    {
        m_nudge = v;
    }

    private void UpdateMovement()
    {
        // Acquire Analog Movement
        m_analogMovement.x = GamePad.GetState(m_controller_index).ThumbSticks.Left.X;
        m_analogMovement.y = GamePad.GetState(m_controller_index).ThumbSticks.Left.Y;
        // Override from DPAD possibility
        if (GamePad.GetState(m_controller_index).DPad.Left == ButtonState.Pressed)
            m_analogMovement.x += -1.0f;
        if (GamePad.GetState(m_controller_index).DPad.Right == ButtonState.Pressed)
            m_analogMovement.x += +1.0f;
        if (GamePad.GetState(m_controller_index).DPad.Up == ButtonState.Pressed)
            m_analogMovement.y += +1.0f;
        if (GamePad.GetState(m_controller_index).DPad.Down == ButtonState.Pressed)
            m_analogMovement.y += -1.0f;
        // Clamp Movement
        m_analogMovement.x = Mathf.Clamp(m_analogMovement.x, -1.0f, 1.0f);
        m_analogMovement.y = Mathf.Clamp(m_analogMovement.y, -1.0f, 1.0f);

        // Update Movement Based on Camera and Analog
        m_playerMovement = Vector3.zero;
        m_playerMovement += m_cameraReference.ForwardXZ * m_analogMovement.y;
        m_playerMovement += m_cameraReference.RightXZ * m_analogMovement.x;
        m_playerMovement.Normalize();

        // Modify Movement based on intensity
        m_playerMovement *= m_analogMovement.magnitude;

        // Move Player
        m_nudgeFixed = Mathfx.ZenoLerp(m_nudgeFixed, m_nudge, 0.8f);

        m_characterController.Move(m_playerMovement * PlayerSpeed + m_nudgeFixed);
        // Rotate player
        transform.LookAt(transform.position + m_playerMovement);

        // Decrease nudge over time
        m_nudge *= NudgeRemover;
        if (m_nudge.magnitude < 0.01f)
            m_nudge = Vector3.zero;

        // Raycast floor and fix Y value
        bool Fail = false;
        float floorY = RaycastSimple.Floor(transform.position, PlayerCollision, ref Fail).y;
        floorY += (m_characterController.height / 2) * transform.localScale.y;
        if (!Fail)
            transform.SetY(floorY);

    }
    private void UpdateActions()
    {
        // Update Buttons
        m_buttonAState[0] = (GamePad.GetState(m_controller_index).Buttons.A == ButtonState.Pressed);
        m_buttonXState[0] = (GamePad.GetState(m_controller_index).Buttons.X == ButtonState.Pressed);

        // Update Commands
        m_buttonInteract = m_buttonAState[0] && !m_buttonAState[1];
        m_buttonAttack = m_buttonXState[0] && !m_buttonXState[1];

        // Update Previous Buttons
        m_buttonAState[1] = m_buttonAState[0];
        m_buttonXState[1] = m_buttonXState[0];

        // Interact
        if (m_buttonInteract)
        {
            // Attempt to Pickup an item
            if (m_holdItem == null)
            {
                // Check if item is near
                Collider[] Colliders = Physics.OverlapSphere(transform.position + transform.forward * 1.0f, 0.1f);
                foreach (Collider C in Colliders)
                {
                    if (C.gameObject.layer == GameData.ItemLayer)
                    {
                        m_holdItem = C.gameObject;
                        m_holdItem.transform.SetParent(transform);
                        m_holdItem.transform.position = Vector3.zero;
                        m_holdItem.transform.localPosition = new Vector3(0, 0, 1);
                        break;
                    }
                   
                }
            }
            // Attempt to Drop Item
            else
            {
                // Check if dropping into Car Zone
                Collider[] Colliders = Physics.OverlapSphere(m_holdItem.transform.position, m_holdItem.GetComponent<SphereCollider>().radius);
                foreach (Collider C in Colliders)
                {
                    if(C.gameObject.CompareTag("Player1Car"))
                    {
                        GameData.Player1Score += C.GetComponent<GameItem>().m_Value;
                        m_holdItem.GetComponent<GameItem>().Explode();

                        // Fx 1
                        GameObject.FindObjectOfType<GameMaster>().Kaching_P1();
                    }
                    else if (C.gameObject.CompareTag("Player2Car"))
                    {
                        GameData.Player2Score += C.GetComponent<GameItem>().m_Value;
                        m_holdItem.GetComponent<GameItem>().Explode();

                        // Fx 2
                        GameObject.FindObjectOfType<GameMaster>().Kaching_P2();
                    }
                }

                m_holdItem.transform.SetParent(null);
                m_holdItem.transform.position = (transform.position + transform.forward);
                m_holdItem.GetComponent<GameItem>().MoveToFloor();

                m_holdItem = null;
            }
        }
        // Attack
        if (m_buttonAttack)
        {
            // PLAYER 1 NUDGING PLAYER 2
            if (m_controller_index == PlayerIndex.One)
            {
                // Can't attack if other player is hurt
                if (GameData.m_player2.IsHurt() == false)
                {
                    Vector3 NudgeDirection = (GameData.m_player2.transform.position - GameData.m_player1.transform.position).normalized;
                    float AttackDistance = ((transform.position + transform.forward) - GameData.m_player2.transform.position).magnitude;

                    if (AttackDistance < NudgeDistanceMin)
                    {
                        // Raycast to see if Nudge reaches
                        if (RaycastSimple.ReachPoint(transform.position, GameData.m_player2.transform.position, PlayerCollision))
                        {
                            GameData.m_player2.GetComponent<GamePlayer>().Nudge(NudgeDirection * NudgeStrength);
                            GameData.m_player2.GetComponent<GamePlayer>().Hurt();
                        }
                    }
                }
            }
            // Vice Versa
            else if (m_controller_index == PlayerIndex.Two)
            {
                // Can't attack if other player is hurt
                if (GameData.m_player1.IsHurt() == false)
                {
                    Vector3 NudgeDirection = (GameData.m_player1.transform.position - GameData.m_player2.transform.position).normalized;
                    float AttackDistance = ((transform.position + transform.forward) - GameData.m_player1.transform.position).magnitude;

                    if (AttackDistance < NudgeDistanceMin)
                    {
                        if (RaycastSimple.ReachPoint(transform.position, GameData.m_player1.transform.position, PlayerCollision))
                        {
                            GameData.m_player1.GetComponent<GamePlayer>().Nudge(NudgeDirection * NudgeStrength);
                            GameData.m_player1.GetComponent<GamePlayer>().Hurt();
                        }
                    }
                }
            }
        }
    }
	
	void Update ()
    {
        if (GameData.GameState == GameData.State.GAME_PLAY)
        {
            UpdateMovement();
            UpdateActions();

            if (m_hurt)
            {
                if (Mathf.Repeat(m_hurtTime, 1.0f) > 0.5f)
                    GetComponent<MeshRenderer>().enabled = false;
                else
                    GetComponent<MeshRenderer>().enabled = true;

                m_hurtTime -= (Time.deltaTime * 10.0f);

                if (m_hurtTime < 0.0f)
                {
                    m_hurt = false;
                    GetComponent<MeshRenderer>().enabled = true;
                }
            }
        }
    }
}
