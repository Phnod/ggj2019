﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(SphereCollider))]
public class GameItem : MonoBehaviour
{
    [System.NonSerialized]
    public SphereCollider m_collider;
    [System.NonSerialized]
    public GameItemSpawner m_parent;

    public int m_Value = 0;

    public TMPro.TextMeshProUGUI ValueText;

    public List<GameObject> m_possibleModels;

    private void Awake()
    {
        m_collider = GetComponent<SphereCollider>();
        if (m_collider == null)
            Debug.LogError("GAME ITEM MISSING SPHERE COLLIDER");

        // Only use one of the possible models
        foreach (GameObject G in m_possibleModels)
            G.SetActive(false);
        int ModelIndex = Random.Range(0, m_possibleModels.Count);
        m_possibleModels[ModelIndex].SetActive(true);
    }

    void Update()
    {
        // Update Money text
        ValueText.text = "$ " + m_Value.ToString();

        // Update money color
        if (m_Value >= 0)
            ValueText.color = Color.green;
        else
            ValueText.color = Color.red;

    }

    public void Explode()
    {
        SpawnSmoke();
        Destroy(gameObject);
        m_parent.RemoveChild();
    }
    public void SpawnSmoke()
    {
        GameObject Smoke = GameObject.Instantiate(GamePrefabs.Prefab_SmokeEffect);
        Smoke.transform.position = transform.position;
    }

    public void MoveToFloor()
    {
        bool Fail = false;
        float floorY = RaycastSimple.Floor(transform.position, ~GameData.CollisionLayer, ref Fail).y;

        if (!Fail)
            transform.SetY(floorY);
    }
}
