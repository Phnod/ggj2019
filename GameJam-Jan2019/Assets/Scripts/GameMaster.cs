﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.UI;

public class GameMaster : MonoBehaviour
{
    void Awake()
    {
        if (Menu == null)
            Debug.LogError("GameMaster: Missing Prefab");
        if (Menu_StartGame == null)
            Debug.LogError("GameMaster: Missing Prefab");
        if (Menu_ExitGame == null)
            Debug.LogError("GameMaster: Missing Prefab");
        if (Menu_SelectionPiece == null)
            Debug.LogError("GameMaster: Missing Prefab");
    }

    void Update()
    {
        switch (GameData.GameState)
        {
            case GameData.State.MENU:
                UpdateMenu();
                break;
            case GameData.State.GAME_SETUP:
                UpdateStartCountdown();
                UpdateGameHud();
                break;
            case GameData.State.GAME_PLAY:
                UpdateGame();
                UpdateGameHud();
                break;
            case GameData.State.GAME_END:
                UpdateEndScreen();
                break;
        }

        // Set Parent Active shortcut
        Menu.SetActive(GameData.GameState == GameData.State.MENU);
        Hud.SetActive(GameData.GameState == GameData.State.GAME_SETUP || GameData.GameState == GameData.State.GAME_PLAY || GameData.GameState == GameData.State.GAME_END);
        Ready_Master.SetActive(GameData.GameState == GameData.State.GAME_SETUP);
        WinScreen_Master.SetActive(GameData.GameState == GameData.State.GAME_END);
    }

    // Menu Stuff
    public GameObject Menu;
    public GameObject Menu_StartGame;
    public GameObject Menu_ExitGame;
    public GameObject Menu_SelectionPiece;
    // Selection Locations
    private Vector3 Selection_StartGame = new Vector3(0, -55, 18);
    private Vector3 Selection_ExitGame = new Vector3(0, -145, 18);
    // Selection Index
    private uint SelectionIndex = 0;

    // Update
    private void UpdateMenu()
    {
        // Up = StartGame
        if (
            GamePad.GetState(PlayerIndex.One).DPad.Up == ButtonState.Pressed ||
            GamePad.GetState(PlayerIndex.Two).DPad.Up == ButtonState.Pressed ||
            GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y > 0.1f ||
            GamePad.GetState(PlayerIndex.Two).ThumbSticks.Left.Y > 0.1f
            )
        {
            SelectionIndex = 0;
        }
        // Down = ExitGame
        if (
            GamePad.GetState(PlayerIndex.One).DPad.Down == ButtonState.Pressed ||
            GamePad.GetState(PlayerIndex.Two).DPad.Down == ButtonState.Pressed ||
            GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y < -0.1f ||
            GamePad.GetState(PlayerIndex.Two).ThumbSticks.Left.Y < -0.1f
            )
        {
            SelectionIndex = 1;
        }

        // Update Menu Pieces
        if (SelectionIndex == 0)
        {
            Menu_SelectionPiece.transform.localPosition = Vector3.Lerp
                (
                Menu_SelectionPiece.transform.localPosition,
                Selection_StartGame,
                0.2f
                );
        }
        else if (SelectionIndex == 1)
        {
            Menu_SelectionPiece.transform.localPosition = Vector3.Lerp
                (
                Menu_SelectionPiece.transform.localPosition,
                Selection_ExitGame,
                0.2f
                );
        }

        // Press A
        if (
            GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Pressed ||
            GamePad.GetState(PlayerIndex.One).Buttons.X == ButtonState.Pressed ||
            GamePad.GetState(PlayerIndex.Two).Buttons.A == ButtonState.Pressed ||
            GamePad.GetState(PlayerIndex.Two).Buttons.X == ButtonState.Pressed
            )
        {
            if (SelectionIndex == 0)
            {
                GameData.GameState = GameData.State.GAME_SETUP;
                StartGame();
            }
            else if (SelectionIndex == 1)
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
            }
        }
    }

    // Game Start Stuff
    public GameObject Ready_Master;
    public GameObject Ready_Counter1;
    public GameObject Ready_Counter2;
    // Game Stuff
    public GameObject Hud;
    public GameObject Hud_P1_Prompt_A;
    public GameObject Hud_P1_Prompt_X;
    public GameObject Hud_P2_Prompt_A;
    public GameObject Hud_P2_Prompt_X;
    public GameObject Hud_Player1Score;
    public GameObject Hud_Player2Score;
    public GameObject Hud_Money1;
    public GameObject Hud_Money2;
    //
    public TimerScript Game_Timer;
    //
    public GameObject WinScreen_Master;
    public GameObject WinScreen_Text_Player1;
    public GameObject WinScreen_Text_Player2;
    public GameObject WinScreen_Text_Number1;
    public GameObject WinScreen_Text_Number2;
    public GameObject WinScreen_Tie1;
    public GameObject WinScreen_Tie2;

    public void Kaching_P1()
    {
        Hud_Money1.GetComponent<DollarEffect>().Kaching();
    }
    public void Kaching_P2()
    {
        Hud_Money2.GetComponent<DollarEffect>().Kaching();
    }

    // Item Spawners
    private List<GameItemSpawner> Spawners = new List<GameItemSpawner>();
    private void SpawnRandomItem()
    {
        if (Spawners.Count == 0)
        {
            Debug.Log("NO SPAWNERS STORED");
            return;
        }

        int RandomIndex = Random.Range(0, Spawners.Count);
        Spawners[RandomIndex].Spawn();
    }

    private void UpdateGameHud()
    {
        // Score
        Hud_Player1Score.GetComponent<Text>().text = "$ " + GameData.Player1Score.ToString();
        Hud_Player2Score.GetComponent<Text>().text = "$ " + GameData.Player2Score.ToString();

        // Button Presses Hud
        float darkness = 75.0f / 255.0f;
        // 
        if (GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Pressed)
            Hud_P1_Prompt_A.GetComponent<Image>().color = new Color(darkness, darkness, darkness, 1);
        else
            Hud_P1_Prompt_A.GetComponent<Image>().color = Color.white;
        //
        if (GamePad.GetState(PlayerIndex.One).Buttons.X == ButtonState.Pressed)
            Hud_P1_Prompt_X.GetComponent<Image>().color = new Color(darkness, darkness, darkness, 1);
        else
            Hud_P1_Prompt_X.GetComponent<Image>().color = Color.white;

        // 
        if (GamePad.GetState(PlayerIndex.Two).Buttons.A == ButtonState.Pressed)
            Hud_P2_Prompt_A.GetComponent<Image>().color = new Color(darkness, darkness, darkness, 1);
        else
            Hud_P2_Prompt_A.GetComponent<Image>().color = Color.white;
        //
        if (GamePad.GetState(PlayerIndex.Two).Buttons.X == ButtonState.Pressed)
            Hud_P2_Prompt_X.GetComponent<Image>().color = new Color(darkness, darkness, darkness, 1);
        else
            Hud_P2_Prompt_X.GetComponent<Image>().color = Color.white;
    }

    private void StartGame()
    {
        Game_Timer.ResetAndPause();
        GameData.GameStartTime = GameData.GameStartCounterTime;
        GameData.Player1Score = 0;
        GameData.Player2Score = 0;
        GameData.GameTime = 0.0f;

        Spawners.Clear();
        GameObject[] Objs = GameObject.FindGameObjectsWithTag("ItemSpawner");
        foreach (GameObject G in Objs)
        {
            if (G.GetComponent<GameItemSpawner>() != null)
                Spawners.Add(G.GetComponent<GameItemSpawner>());
        }

        // Init TEST
        foreach (GameItemSpawner G in Spawners)
            G.Spawn();
    }
    private void UpdateStartCountdown()
    {
        string TimeLeft = Mathf.FloorToInt(GameData.GameStartTime).ToString();
        Ready_Counter1.GetComponent<Text>().text = TimeLeft;
        Ready_Counter2.GetComponent<Text>().text = TimeLeft;

        GameData.GameStartTime -= Time.deltaTime;

        if(GameData.GameStartTime <= 0.0f)
        {
            GameData.GameState = GameData.State.GAME_PLAY;
            Game_Timer.StartTimer();
        }
    }
    private void UpdateGame()
    {
        // Timer
        GameData.GameTime += Time.deltaTime;

        if(GameData.GameTime >= 3.0f)
        {
            //SpawnRandomItem();
            GameData.GameTime -= 1.0f;
        }

        // Check if Stephen Timer is complete
        if(Game_Timer.IsComplete())
        {
            GameData.GameState = GameData.State.GAME_END;
        }
    }
    private void UpdateEndScreen()
    {
        // Destroy all existing Items
        GameItem[] g = GameObject.FindObjectsOfType<GameItem>();
        foreach (GameItem item in g)
        {
            item.Explode();
        }
        // Teleport Players back to start
        GamePlayer[] p = GameObject.FindObjectsOfType<GamePlayer>();
        foreach (GamePlayer player in p)
        {
            player.transform.position = player.m_spawnPosition;
        }
        // Update Win Screen Colors
        if (GameData.Player1Score > GameData.Player2Score)
        {
            WinScreen_Text_Player1.SetActive(true);
            WinScreen_Text_Number1.SetActive(true);
            WinScreen_Text_Player2.SetActive(true);
            WinScreen_Text_Number2.SetActive(true);
            WinScreen_Tie1.SetActive(false);
            WinScreen_Tie2.SetActive(false);

            WinScreen_Text_Player1.GetComponent<Text>().color = Color.blue;
            WinScreen_Text_Number1.GetComponent<Text>().color = Color.blue;

            WinScreen_Text_Number1.GetComponent<Text>().text = "1";
            WinScreen_Text_Number2.GetComponent<Text>().text = "1";
        }
        else if (GameData.Player1Score < GameData.Player2Score)
        {
            WinScreen_Text_Player1.SetActive(true);
            WinScreen_Text_Number1.SetActive(true);
            WinScreen_Text_Player2.SetActive(true);
            WinScreen_Text_Number2.SetActive(true);
            WinScreen_Tie1.SetActive(false);
            WinScreen_Tie2.SetActive(false);

            WinScreen_Text_Player1.GetComponent<Text>().color = Color.red;
            WinScreen_Text_Number1.GetComponent<Text>().color = Color.red;

            WinScreen_Text_Number1.GetComponent<Text>().text = "2";
            WinScreen_Text_Number2.GetComponent<Text>().text = "2";
        }
        else
        {
            WinScreen_Text_Player1.SetActive(false);
            WinScreen_Text_Number1.SetActive(false);
            WinScreen_Text_Player2.SetActive(false);
            WinScreen_Text_Number2.SetActive(false);
            WinScreen_Tie1.SetActive(true);
            WinScreen_Tie2.SetActive(true);
        }
        // Press X to restart
        if(
            GamePad.GetState(PlayerIndex.One).Buttons.Y == ButtonState.Pressed ||
            GamePad.GetState(PlayerIndex.Two).Buttons.Y == ButtonState.Pressed
            )
        {
            GameData.GameState = GameData.State.GAME_SETUP;
            StartGame();
        }
    }
}
