﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class GameCamera : MonoBehaviour
{
    // Data
    private GamePlayer m_player1 = null;
    private GamePlayer m_player2 = null;
    private Vector3 m_playerOffset = new Vector3(0, 17.5f, -9.0f);
    private Vector3 m_targetPosition;
    private Camera m_camera;

    // Public
    [System.NonSerialized]
    public Vector3 ForwardXZ;
    [System.NonSerialized]
    public Vector3 RightXZ;

    public float Zoom_MinDistApart = 0.0f;
    public float Zoom_MaxDistApart = 1.0f;
    public float Zoom_MinZoom = 0.0f;
    public float Zoom_MaxZoom = 1.0f;

    void Start()
    {
        m_camera = GetComponent<Camera>();

        m_player1 = GameData.m_player1;
        m_player2 = GameData.m_player2;

        if (m_player1 == null)
            Debug.LogError("CAMERA: CANNOT FIND PLAYER1");
        if (m_player2 == null)
            Debug.LogWarning("CAMERA: CANNOT FIND PLAYER2");

        GameData.m_camera = this;
    }

    private void FollowPlayer()
    {
        // Update Public
        ForwardXZ = transform.forward;
        ForwardXZ.y = 0;
        if (ForwardXZ.sqrMagnitude != 0)
            ForwardXZ.Normalize();

        RightXZ = transform.right;
        RightXZ.y = 0;
        if (RightXZ.sqrMagnitude != 0)
            RightXZ.Normalize();

        // Position for self
        Vector3 TargetPosition;
        if (m_player2 != null)
            TargetPosition = (m_player1.transform.position + m_player2.transform.position) / 2.0f;
        else
            TargetPosition = m_player1.transform.position;

        m_targetPosition = Vector3.Lerp(m_targetPosition, TargetPosition, 0.2f);

        Vector3 CameraPosition = m_targetPosition + m_playerOffset;

        // Update Position
        transform.position = CameraPosition;

        // Update Size
        if (m_player2 == null)
            m_camera.orthographicSize = 30.0f;
        else
        {
            float TargetZoom;

            float DistanceApart = (GameData.m_player1.transform.position - GameData.m_player2.transform.position).magnitude;
            if(DistanceApart < Zoom_MinDistApart)
                TargetZoom = Zoom_MinZoom;
            else if(DistanceApart > Zoom_MaxDistApart)
                TargetZoom = Zoom_MaxZoom;
            else
            {
                float t = (DistanceApart - Zoom_MinDistApart) / (Zoom_MaxDistApart - Zoom_MinDistApart);
                TargetZoom = Mathf.Lerp(Zoom_MinZoom, Zoom_MaxZoom, t);
            }

            m_camera.orthographicSize = Mathf.Lerp(m_camera.orthographicSize, TargetZoom, 0.03f);
        }
    }
    private void MenuSpot()
    {
        transform.position = Vector3.Lerp(transform.position, new Vector3(0, 17.5f, -9.0f), 0.2f);
    }
    private void EndSpot()
    {
        transform.position = Vector3.Lerp(transform.position, new Vector3(0, 31.0f, -36.0f), 0.2f);
    }

    void Update()
    {
        switch(GameData.GameState)
        {
            case GameData.State.GAME_END:
                EndSpot();
                break;

            case GameData.State.MENU:
                MenuSpot();
                break;

            case GameData.State.GAME_SETUP:
            case GameData.State.GAME_PLAY:
                FollowPlayer();
                break;
        }

        
    }
}
