﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerScript : MonoBehaviour
{
    private float timerStart;
    public float currentTime;
    bool _timerActive = false;
    public TMPro.TextMeshProUGUI timerMesh;
    // Start is called before the first frame update
    void Start()
    {
        timerStart = GameData.GameCountdownTime;
        if (timerMesh == null)
        {
            timerMesh = GetComponent<TMPro.TextMeshProUGUI>();
        }
        currentTime = timerStart;
    }

    public void StartTimer()
    {
        _timerActive = true;
    }
    public void PauseTimer()
    {
        _timerActive = false;
    }
    public void ResetAndPause()
    {
        currentTime = timerStart;
        _timerActive = false;
    }
    public bool IsComplete()
    {
        return currentTime <= 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if(_timerActive)
        {
            currentTime -= Time.deltaTime;
            if(currentTime <= 0.0f)
            {
                _timerActive = false;
                currentTime = 0.0f;
                // Do Action
            }
        }
        timerMesh.text = CalculateTimeString(currentTime);
    }

    private string CalculateTimeString(float timer)
    {
        float msec = Mathf.Floor((timer * 100) % 100); // * 1000.0f;
        float seconds = Mathf.Floor(timer % 60);
        float minutes = Mathf.Floor((timer / 60) % 60);

        string milliStr;
        string secondsStr;
        string minutesStr;

        string blinkingColon = ":";
        if (msec > 50)
            blinkingColon = " ";

        minutesStr = DoubleDigitConvert(minutes);
        secondsStr = DoubleDigitConvert(seconds); 
        milliStr = DoubleDigitConvert(msec);

        string text =
            string.Format("{00}", minutesStr) + blinkingColon +
            string.Format("{00}.", secondsStr) +
            string.Format("{00}\n", milliStr);

        return text;
    }

    private string DoubleDigitConvert(float num)
    {
        if (num < 10)
            return "0" + Mathf.RoundToInt(Mathf.Floor(num)).ToString();
        else
            return Mathf.RoundToInt(Mathf.Floor(num)).ToString();
    }
}
