﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePrefabs : MonoBehaviour
{
    // Publics
    public GameObject Item;
    public GameObject SmokeEffect;

    // Statics
    public static GameObject Prefab_Item;
    public static GameObject Prefab_SmokeEffect;


    // Setter
    public void Set<Type>(Type src, ref Type dest)
    {
        if (src == null)
            Debug.LogError("Prefab in PrefabManager is Missing");

        dest = src;
    }

    // Connect
    private void Awake()
    {
        // Setterss
        Set(Item, ref Prefab_Item);
        Set(SmokeEffect, ref Prefab_SmokeEffect);

    }
}
