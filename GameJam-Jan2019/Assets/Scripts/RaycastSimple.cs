﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class OverlapSafe
{
    public static Collider[] Box(Vector3 center, Vector3 halfExtents, Quaternion quat, LayerMask mask)
    {
        return Physics.OverlapBox(center, halfExtents, quat, mask.value);
    }
    public static Collider[] Sphere(Vector3 position, float radius, LayerMask mask)
    {
        return Physics.OverlapSphere(position, radius, mask.value);
    }
    public static Collider[] Capsule(Vector3 point0, Vector3 point1, float radius, LayerMask mask)
    {
        return Physics.OverlapCapsule(point0, point1, radius, mask.value);
    }
}

public static class RaycastSimple
{
    // Raycast Mouse to Scene
    public static GameObject MouseToSceneGameObject()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            return hit.transform.gameObject;
        }
        return null;
    }
    public static GameObject MouseToSceneGameObject(LayerMask Layer)
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if(Physics.Raycast(ray, out hit, Mathf.Infinity, Layer))
        {
            return hit.transform.gameObject;
        }
        return null;
    }
    public static Vector3 MouseToScenePosition()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            return hit.point;
        }
        return Vector3.zero;
    }
    public static Vector3 MouseToScenePosition(LayerMask Layer)
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, Layer))
        {
            return hit.point;
        }
        return Vector3.zero;
    }

    // Check if Raycast Reaches Point
    public static bool ReachPoint(Vector3 Start, Vector3 End, LayerMask Layer)
    {
        // Raycast Data
        RaycastHit hit;
        float distance = (End - Start).magnitude;
        Ray ray = new Ray(Start, (End - Start).normalized);

        if (Physics.Raycast(ray, out hit, distance * 2.0f, Layer))
        {
            // Check distance
            float raydistance = (hit.point - Start).magnitude;

            //Debug.Log(hit.point);
            //Debug.Log("Distance to reach: " + distance);
            //Debug.Log("Raycast distance: " + raydistance);

            //Debug.DrawLine(Start + Vector3.up * 3.0f, hit.point + Vector3.up * 3.0f, Color.red);

            // If raydistance ended up closer, it didn't reach
            if (raydistance < distance - 0.005f)
                return false;

            // Else it did reach the point
            return true;
        }

        //Debug.Log("Raycast returned nothing");
        return true;
    }

    // Raycast Data
    public static RaycastHit Ray(Vector3 Start, Vector3 Direction)
    {
        // Raycast Data
        RaycastHit hit;
        Ray ray = new Ray(Start, Direction);

        if (Physics.Raycast(ray, out hit))
        {
            return hit;
        }

        return hit;
    }
    public static RaycastHit Ray(Vector3 Start, Vector3 Direction, LayerMask Mask)
    {
        // Raycast Data
        RaycastHit hit;
        Ray ray = new Ray(Start, Direction);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, Mask))
        {
            return hit;
        }

        return hit;
    }
    public static RaycastHit Ray(Vector3 Start, Vector3 Direction, LayerMask Mask, ref bool Fail)
    {
        // Raycast Data
        RaycastHit hit;
        Ray ray = new Ray(Start, Direction);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, Mask))
        {
            Fail = false;
            return hit;
        }

        Fail = true;
        return hit;
    }

    // Raycast but only point
    public static Vector3 Point(Vector3 Start, Vector3 Direction)
    {
        // Raycast Data
        RaycastHit hit;
        Ray ray = new Ray(Start, Direction);

        if (Physics.Raycast(ray, out hit))
        {
            return hit.point;
        }

        return Vector3.zero;
    }
    public static Vector3 Point(Vector3 Start, Vector3 Direction, LayerMask Layer)
    {
        // Raycast Data
        RaycastHit hit;
        Ray ray = new Ray(Start, Direction);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, Layer))
        {
            return hit.point;
        }

        return Vector3.zero;
    }
    public static Vector3 Point(Vector3 Start, Vector3 Direction, LayerMask Layer, ref bool Fail)
    {
        // Raycast Data
        RaycastHit hit;
        Ray ray = new Ray(Start, Direction);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, Layer))
        {
            Fail = false;
            return hit.point;
        }

        Fail = true;
        return Vector3.zero;
    }

    // Raycast Floor
    public static Vector3 Floor(Vector3 Start)
    {
        return Point(Start, Vector3.down);
    }
    public static Vector3 Floor(Vector3 Start, LayerMask Layer)
    {
        return Point(Start, Vector3.down, Layer);
    }
    public static Vector3 Floor(Vector3 Start, LayerMask Layer, ref bool Fail)
    {
        return Point(Start, Vector3.down, Layer, ref Fail);
    }

    // Raycast Ceiling
    public static Vector3 Ceiling(Vector3 Start)
    {
        return Point(Start, Vector3.up);
    }
    public static Vector3 Ceiling(Vector3 Start, LayerMask Layer)
    {
        return Point(Start, Vector3.up, Layer);
    }
    public static Vector3 Ceiling(Vector3 Start, LayerMask Layer, ref bool Fail)
    {
        return Point(Start, Vector3.up, Layer, ref Fail);
    }

}

public static class SpherecastSimple
{
    // Check if touching a collider
    public static bool IsTouchingCollider(Vector3 Point, float radius)
    {
        Collider[] C = Physics.OverlapSphere(Point, radius);
        return (C.Length != 0);
    }
    public static bool IsTouchingCollider(Vector3 Point, float radius, LayerMask Layer)
    {
        Collider[] C = OverlapSafe.Sphere(Point, radius, Layer);
        return (C.Length != 0);
    }

    // Check if Raycast Reaches Point
    public static bool ReachPoint(Vector3 Start, Vector3 End, float radius, LayerMask Layer)
    {
        // Raycast Data
        RaycastHit hit;
        float distance = (End - Start).magnitude;
        Ray ray = new Ray(Start, (End - Start).normalized);

        if (Physics.SphereCast(ray, radius, out hit, distance * 2.0f, Layer))
        {
            // Check distance
            float raydistance = (hit.point - Start).magnitude;

            // If raydistance ended up closer, it didn't reach
            if (raydistance < distance - 0.005f)
                return false;

            // Else it did reach the point
            return true;
        }

        //Debug.Log("Raycast returned nothing");
        return true;
    }

    // Spherecast Data
    public static RaycastHit Ray(Vector3 Start, Vector3 Direction, float radius)
    {
        // Raycast Data
        RaycastHit hit;
        Ray ray = new Ray(Start, Direction);

        if (Physics.SphereCast(ray, radius, out hit))
        {
            return hit;
        }

        return hit;
    }
    public static RaycastHit Ray(Vector3 Start, Vector3 Direction, float radius, LayerMask Mask)
    {
        // Raycast Data
        RaycastHit hit;
        Ray ray = new Ray(Start, Direction);

        if (Physics.SphereCast(ray, radius, out hit, Mathf.Infinity, Mask))
        {
            return hit;
        }

        return hit;
    }
    public static RaycastHit Ray(Vector3 Start, Vector3 Direction, float radius, LayerMask Mask, ref bool Fail)
    {
        // Raycast Data
        RaycastHit hit;
        Ray ray = new Ray(Start, Direction);

        if (Physics.SphereCast(ray, radius, out hit, Mathf.Infinity, Mask))
        {
            Fail = false;
            return hit;
        }

        Fail = true;
        return hit;
    }

    // Spherecast but only point
    public static Vector3 Point(Vector3 Start, Vector3 Direction, float radius)
    {
        // Raycast Data
        RaycastHit hit;
        Ray ray = new Ray(Start, Direction);

        if (Physics.SphereCast(ray, radius, out hit))
        {
            return hit.point;
        }

        return Vector3.zero;
    }
    public static Vector3 Point(Vector3 Start, Vector3 Direction, float radius, LayerMask Layer)
    {
        // Raycast Data
        RaycastHit hit;
        Ray ray = new Ray(Start, Direction);

        if (Physics.SphereCast(ray, radius, out hit, Mathf.Infinity, Layer))
        {
            return hit.point;
        }

        return Vector3.zero;
    }
    public static Vector3 Point(Vector3 Start, Vector3 Direction, float radius, LayerMask Layer, ref bool Fail)
    {
        // Raycast Data
        RaycastHit hit;
        Ray ray = new Ray(Start, Direction);

        if (Physics.SphereCast(ray, radius, out hit, Mathf.Infinity, Layer))
        {
            Fail = false;
            return hit.point;
        }

        Fail = true;
        return Vector3.zero;
    }

    // Spherecast Floor
    public static Vector3 Floor(Vector3 Start, float radius)
    {
        return Point(Start, Vector3.down, radius);
    }
    public static Vector3 Floor(Vector3 Start, float radius, LayerMask Layer)
    {
        return Point(Start, Vector3.down, radius, Layer);
    }
    public static Vector3 Floor(Vector3 Start, float radius, LayerMask Layer, ref bool Fail)
    {
        return Point(Start, Vector3.down, radius, Layer, ref Fail);
    }

    // Spherecast Ceiling
    public static Vector3 Ceiling(Vector3 Start, float radius)
    {
        return Point(Start, Vector3.up, radius);
    }
    public static Vector3 Ceiling(Vector3 Start, float radius, LayerMask Layer)
    {
        return Point(Start, Vector3.up, radius, Layer);
    }
    public static Vector3 Ceiling(Vector3 Start, float radius, LayerMask Layer, ref bool Fail)
    {
        return Point(Start, Vector3.up, radius, Layer, ref Fail);
    }

}