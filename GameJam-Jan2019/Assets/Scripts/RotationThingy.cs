﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationThingy : MonoBehaviour
{
    void Update()
    {
        float t = Mathf.Cos(Time.time * 4.0f) * 0.5f + 0.5f;
        Vector3 Euler = transform.eulerAngles;
        Euler.z = Mathf.Lerp(12.0f, -20.0f, t);
        transform.eulerAngles = Euler;
    }
}
