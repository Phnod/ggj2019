﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameItemSpawner : MonoBehaviour
{
    int[] Values =
    {
        -300,
        100,
        200,
        250,
        300,
        400,
        800
    };

    int GetRandomValue()
    {
        return Values[Random.Range(0, Values.Length)];
    }

    private GameItem m_child = null;
    public void RemoveChild()
    {
        m_child = null;
    }

    public void Spawn()
    {
        if (m_child == null)
        {
            // Create Item
            GameObject Item = GameObject.Instantiate(GamePrefabs.Prefab_Item);
            Item.transform.position = transform.position;
            Item.GetComponent<GameItem>().m_Value = GetRandomValue();
            Item.GetComponent<GameItem>().MoveToFloor();
            Item.GetComponent<GameItem>().m_parent = this;

            // Remember
            m_child = Item.GetComponent<GameItem>();
        }
    }
}
