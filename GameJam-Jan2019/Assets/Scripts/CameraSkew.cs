﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSkew : MonoBehaviour
{
	Camera reference;
    public Vector2 cameraSkew = new Vector2(0.01f, 0.01f);

    // Start is called before the first frame update
    void Start()
    {
        reference = this.GetComponent<Camera>();
        SetObliqueness(cameraSkew);
    }

    void SetObliqueness(Vector2 skew) 
	{
        Matrix4x4 mat  = reference.projectionMatrix;
        mat[0, 2] = skew.x;
        mat[1, 2] = skew.y;
        reference.projectionMatrix = mat;
    }

	void SetObliqueness(float horizObl, float vertObl) 
	{
        Matrix4x4 mat  = reference.projectionMatrix;
        mat[0, 2] = horizObl;
        mat[1, 2] = vertObl;
        reference.projectionMatrix = mat;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
