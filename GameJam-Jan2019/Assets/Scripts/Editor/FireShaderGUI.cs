﻿using UnityEngine;
using UnityEngine.Rendering;
using UnityEditor;

public class FireShaderGUI : ShaderGUI 
{

	enum RenderingMode 
	{
		Opaque, Cutout, Fade, Transparent
	}

	enum CullingMode 
    {
		Off = 0, Front = 1, Back = 2
	}

	struct RenderingSettings 
	{
		public RenderQueue queue;
		public string renderType;
		public BlendMode srcBlend, dstBlend;
		public bool zWrite;

		public static RenderingSettings[] modes = 
		{
			new RenderingSettings() 
			{
				queue = RenderQueue.Geometry,
				renderType = "",
				srcBlend = BlendMode.One,
				dstBlend = BlendMode.Zero,
				zWrite = true
			},
			new RenderingSettings() 
			{
				queue = RenderQueue.AlphaTest,
				renderType = "TransparentCutout",
				srcBlend = BlendMode.One,
				dstBlend = BlendMode.Zero,
				zWrite = true
			},
			new RenderingSettings() 
			{
				queue = RenderQueue.Transparent,
				renderType = "Transparent",
				srcBlend = BlendMode.SrcAlpha,
				dstBlend = BlendMode.OneMinusSrcAlpha,
				zWrite = false
			},
			new RenderingSettings() 
			{
				queue = RenderQueue.Transparent,
				renderType = "Transparent",
				srcBlend = BlendMode.One,
				dstBlend = BlendMode.OneMinusSrcAlpha,
				zWrite = false
			}
		};
	}

	struct CullSettings 
    {
		public CullMode cull;
		public static CullSettings[] modes = 
        {
			new CullSettings() 
            {
				cull = CullMode.Off
			},
			new CullSettings() 
            {
				cull =  CullMode.Front
			},
			new CullSettings() 
            {
				cull =  CullMode.Back
			}
		};
	}

	static GUIContent staticLabel = new GUIContent();

	Material target;
	MaterialEditor editor;
	MaterialProperty[] properties;
	bool shouldShowAlphaCutoff;
	bool shouldShowSoftParticle;

	public override void OnGUI (
		MaterialEditor editor, MaterialProperty[] properties) 
	{
		this.target = editor.target as Material;
		this.editor = editor;
		this.properties = properties;
		DoRenderingMode();
		DoMain();
		DoCloud();
		GUILayout.Label("Settings", EditorStyles.boldLabel);
		DoDarkEdge();
		DoOffset();
	}

	void DoRenderingMode () 
	{
		RenderingMode mode = RenderingMode.Opaque;
		shouldShowAlphaCutoff = false;
		shouldShowSoftParticle = false;
		if (IsKeywordEnabled("_RENDERING_CUTOUT")) 
		{
			mode = RenderingMode.Cutout;
			shouldShowAlphaCutoff = true;
		}
		else if (IsKeywordEnabled("_RENDERING_FADE")) 
		{
			mode = RenderingMode.Fade;
			shouldShowSoftParticle = true;
		}
		else if (IsKeywordEnabled("_RENDERING_TRANSPARENT")) 
		{
			mode = RenderingMode.Transparent;
			shouldShowSoftParticle = true;
		}

		EditorGUI.BeginChangeCheck();
		mode = (RenderingMode)EditorGUILayout.EnumPopup(
			MakeLabel("Rendering Mode"), mode
		);
		if (EditorGUI.EndChangeCheck()) 
		{
			RecordAction("Rendering Mode");
			SetKeyword("_RENDERING_CUTOUT", mode == RenderingMode.Cutout);
			SetKeyword("_RENDERING_FADE", mode == RenderingMode.Fade);
			SetKeyword("_RENDERING_TRANSPARENT", mode == RenderingMode.Transparent);

			RenderingSettings settings = RenderingSettings.modes[(int)mode];
			foreach (Material m in editor.targets) 
			{
				m.renderQueue = (int)settings.queue;
				m.SetOverrideTag("RenderType", settings.renderType);
				m.SetInt("_SrcBlend", (int)settings.srcBlend);
				m.SetInt("_DstBlend", (int)settings.dstBlend);
				m.SetInt("_ZWrite", settings.zWrite ? 1 : 0);
			}
		}	
		
		if(shouldShowSoftParticle)
		{
			DoParticleAlpha(); 
		}

		if (mode == RenderingMode.Fade || mode == RenderingMode.Transparent) 
		{
			// Not ready yet
			// DoSemitransparentShadows();
		}	
	}

	void DoCullingMode()
	{
		MaterialProperty type = FindProperty("_Cull");
		CullingMode mode = (CullingMode)type.floatValue;

		EditorGUI.BeginChangeCheck();
		mode = (CullingMode)EditorGUILayout.EnumPopup(
			MakeLabel("Culling Mode"), mode);
		if (EditorGUI.EndChangeCheck()) 
        {
			RecordAction("Culling Mode");

			CullSettings settings = CullSettings.modes[(int)mode];
			foreach (Material m in editor.targets) 
            {
				m.SetInt("_Cull", (int)settings.cull);
			}
		}
	}

	void DoSemitransparentShadows () 
	{
		EditorGUI.BeginChangeCheck();
		bool semitransparentShadows =
			EditorGUILayout.Toggle(
				MakeLabel("Semitransp. Shadows", "Semitransparent Shadows"),
				IsKeywordEnabled("_SEMITRANSPARENT_SHADOWS"));
		if (EditorGUI.EndChangeCheck()) 
		{
			SetKeyword("_SEMITRANSPARENT_SHADOWS", semitransparentShadows);
		}
		if (!semitransparentShadows) 
		{
			shouldShowAlphaCutoff = true;
		}
	}

	void DoMain () 
	{

		GUILayout.Label("Main Maps", EditorStyles.boldLabel);

		MaterialProperty mainTex = FindProperty("_MainTex");
		editor.TexturePropertySingleLine(
			MakeLabel(mainTex, "Albedo (RGB)"), mainTex, FindProperty("_Color"));
			
		if (shouldShowAlphaCutoff) 
		{
			DoAlphaCutoff();
		}
		
		editor.TextureScaleOffsetProperty(mainTex);
		
		
	}

	void DoCloud()
	{
		GUILayout.Label("Noise Map", EditorStyles.boldLabel);
		MaterialProperty cloudTex = FindProperty("_CloudTex");
		editor.TexturePropertySingleLine(MakeLabel(cloudTex, "Cloud Tex"), cloudTex, FindProperty("_TimeSpeed"));
		MaterialProperty cutoffCloudSlider = FindProperty("_CloudCutoff");
		EditorGUI.indentLevel += 2;
		editor.ShaderProperty(cutoffCloudSlider, MakeLabel(cutoffCloudSlider));
		EditorGUI.indentLevel -= 2;
		editor.TextureScaleOffsetProperty(cloudTex);

	}

	void DoAlphaCutoff () 
	{
		MaterialProperty slider = FindProperty("_AlphaCutoff");
		EditorGUI.indentLevel += 2;
		editor.ShaderProperty(slider, MakeLabel(slider));
		EditorGUI.indentLevel -= 2;
	}

	void DoParticleAlpha () 
	{
		EditorGUI.BeginChangeCheck();
		bool softPart =
			EditorGUILayout.Toggle(
				MakeLabel("Soft Particles", "Makes the edges of the flames darker"),
				IsKeywordEnabled("_FIRE_SOFT"));
		if (EditorGUI.EndChangeCheck()) 
		{
			SetKeyword("_FIRE_SOFT", softPart);
		}
		if(softPart)
		{
			MaterialProperty slider = FindProperty("_InvFade");
			EditorGUI.indentLevel += 1;
			editor.ShaderProperty(slider, MakeLabel(slider));
			EditorGUI.indentLevel -= 1;
		}
		
	}

	void DoDarkEdge()
	{
		EditorGUI.BeginChangeCheck();
		bool darkEdges =
			EditorGUILayout.Toggle(
				MakeLabel("Dark Edges", "Makes the edges of the flames darker"),
				IsKeywordEnabled("_FIRE_DARK_EDGE"));
		if (EditorGUI.EndChangeCheck()) 
		{
			SetKeyword("_FIRE_DARK_EDGE", darkEdges);
		}
	}

	void DoOffset()
	{
		MaterialProperty offsetAmount = FindProperty("_OffsetAmount");
		MaterialProperty offsetAngle = FindProperty("_OffsetAngle");
		Vector2 offset = new Vector2
		(
			offsetAngle.floatValue,
			offsetAmount.floatValue
		);

		Vector2 newOffset = EditorGUILayout.Vector2Field(MakeLabel("Z-Offset", "Y is the amount of Offset, with X influencing based on surface angle"), offset);
		
		if(newOffset != offset)
		{
			Debug.Log("New Offset");
			foreach (Material m in editor.targets) 
            {
				offsetAngle.floatValue = newOffset.x;
				offsetAmount.floatValue = newOffset.y;
			}
		}
	}

	MaterialProperty FindProperty (string name) 
	{
		return FindProperty(name, properties);
	}

	static GUIContent MakeLabel (string text, string tooltip = null) 
	{
		staticLabel.text = text;
		staticLabel.tooltip = tooltip;
		return staticLabel;
	}

	static GUIContent MakeLabel (MaterialProperty property, string tooltip = null) 
	{
		staticLabel.text = property.displayName;
		staticLabel.tooltip = tooltip;
		return staticLabel;
	}

	void SetKeyword (string keyword, bool state) 
	{
		if (state) 
		{
			foreach (Material m in editor.targets) 
			{
				m.EnableKeyword(keyword);
			}
		}
		else {
			foreach (Material m in editor.targets) 
			{
				m.DisableKeyword(keyword);
			}
		}
	}

	bool IsKeywordEnabled (string keyword) 
	{
		return target.IsKeywordEnabled(keyword);
	}

	void RecordAction (string label) 
	{
		editor.RegisterPropertyChangeUndo(label);
	}
}