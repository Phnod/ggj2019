﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static class GameData
{
    static public GamePlayer m_player1 = null;
    static public GamePlayer m_player2 = null;
    static public GameCamera m_camera = null;

    static public int ItemLayer = LayerMask.NameToLayer("Item");
    static public int CollisionLayer = LayerMask.NameToLayer("PlayerCollision");

    static public int Player1Score = 0;
    static public int Player2Score = 0;

    static public float GameStartTime = 0.0f;
    static public float GameTime = 0.0f;

    static public float GameCountdownTime = 180.0f; // Seconds you have to play the game before times up (180 for gamejam)
    static public float GameStartCounterTime = 2.0f; // Countdown in Game_Setup before gam starts (set to 5 at end of gamejam plz)

    public enum State
    {
        MENU,
        GAME_SETUP,
        GAME_PLAY,
        GAME_END
    }
    public static State GameState = State.MENU;
}
