﻿Shader "Stephen/GlowRays"
{
	Properties
	{
		_CloudTex ("Cloud", 2D) = "grey" {}
		_CloudParams("Cloud Speed & Scale", Vector) = (1,1,1,1)		
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Color", Color) = (1,1,1,1)
		[HDR]_GlowColor ("Glow Color", Color) = (1,1,1)
		_RampPower ("Ramp Power", Float) = 1
	}
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		LOD 100
		ZWrite Off
		Blend SrcAlpha One

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _CloudTex;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _CloudParams;
			float4 _Color;
			float3 _GlowColor;
			float _RampPower;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = pow(tex2D(_MainTex, i.uv), _RampPower) * _Color;

				float2 timeOffset = _Time.r * _CloudParams.xy;
				float cloud = 	tex2D(_CloudTex, _CloudParams.zw * (i.uv + timeOffset));
				cloud += 		tex2D(_CloudTex, _CloudParams.zw * (i.uv + timeOffset * float2(-1.0, -0.9) + float2(0.50, 0.10)));
				cloud += 		tex2D(_CloudTex, _CloudParams.zw * (i.uv + timeOffset * float2(-1.5,  1.5) + float2(0.75, 0.20)));
				cloud += 		tex2D(_CloudTex, _CloudParams.zw * (i.uv + timeOffset * float2(1.5,  -1.5) + float2(0.25, 0.30)));
				//cloud = (cloud * 0.25);
				

				col.rgb *= _GlowColor * cloud;

				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
