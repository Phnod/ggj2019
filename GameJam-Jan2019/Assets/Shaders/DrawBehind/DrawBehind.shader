﻿Shader "Unlit/DrawBehind"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Color ("Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_Speed ("Speed", Vector) = (1.0, 1.0, 0, 0)
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" }
		LOD 100
		ZWrite Off
		ZTest Greater
		Offset 0, -0.01

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

			// note: no SV_POSITION in this struct
			struct v2f 
			{
				float2 uv : TEXCOORD0;
			};

			v2f vert(
				float4 vertex : POSITION, // vertex position input
				float2 uv : TEXCOORD0, // texture coordinate input
				out float4 outpos : SV_POSITION // clip space position output
				)
			{
				v2f o;
				o.uv = uv;
				outpos = UnityObjectToClipPos(vertex);
				return o;
			}

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float2 _Speed;
			float4 _Color;


            fixed4 frag (v2f i, UNITY_VPOS_TYPE screenPos : VPOS) : SV_Target
            {
				float aspectRatio = _ScreenParams.y / _ScreenParams.x;
				float2 location = screenPos.xy / _ScreenParams.xy;
				location.y *= aspectRatio.r;
				location *= _MainTex_ST.xy; location += _MainTex_ST.zw;
				
                // sample the texture
				float4 col = _Color;
				clip(tex2D(_MainTex, location + _Time.rr * _Speed).r - 0.5);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
