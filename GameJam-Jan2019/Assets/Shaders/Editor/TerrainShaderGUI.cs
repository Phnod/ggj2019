﻿using UnityEngine;
using UnityEngine.Rendering;
using UnityEditor;

public class TerrainShaderGUI : ShaderGUI 
{
	enum RenderingMode 
    {
		Multiplicative, AlphaBlending, Additive, TryThis
	}

	struct RenderingSettings 
    {
		public RenderQueue queue;
		public string renderType;
		public BlendMode srcBlend, dstBlend;
		public bool zWrite;

		public static RenderingSettings[] modes = 
        {
			new RenderingSettings() 
            {
				srcBlend = BlendMode.DstColor,
				dstBlend = BlendMode.Zero,
			},
			new RenderingSettings() 
            {
				srcBlend = BlendMode.SrcAlpha,
				dstBlend = BlendMode.OneMinusSrcAlpha,
			},
			new RenderingSettings() 
            {
				srcBlend = BlendMode.One,
				dstBlend = BlendMode.One,
			},
			new RenderingSettings() 
            {
				srcBlend = BlendMode.OneMinusDstColor,
				dstBlend = BlendMode.OneMinusSrcColor,
			}
		};
	}

	
	static GUIContent staticLabel = new GUIContent();

	static ColorPickerHDRConfig sheenConfig =
		new ColorPickerHDRConfig(0f, 99f, 1f / 99f, 3f);

	Material target;
	MaterialEditor editor;
	MaterialProperty[] properties;

	public override void OnGUI (MaterialEditor editor, MaterialProperty[] properties) 
    {
		this.target = editor.target as Material;
		this.editor = editor;
		this.properties = properties;
		DoRenderingMode();
		DoMain();
        DoColorSplit();
		DoShiny();
		DoOffset();
	}

	void DoRenderingMode () 
    {
		MaterialProperty blendSettingProperty = FindProperty("_BlendSetting");
		int blendSetting = (int)blendSettingProperty.floatValue;

		RenderingMode blendEnum = (RenderingMode)blendSetting;

		int newBlendSetting = System.Convert.ToInt32(EditorGUILayout.EnumPopup(MakeLabel("Blend Type"), blendEnum));
		
		if(newBlendSetting != blendSetting)
		{
			RecordAction("Rendering Mode");

			RenderingSettings settings = RenderingSettings.modes[(int)newBlendSetting];
			foreach (Material m in editor.targets) 
            {
				m.SetInt("_BlendSetting", newBlendSetting);
				m.SetInt("_SrcBlend", (int)settings.srcBlend);
				m.SetInt("_DstBlend", (int)settings.dstBlend);
			}
		}
	}

	void DoMain () 
    {
		GUILayout.Label("Main Maps", EditorStyles.boldLabel);

		MaterialProperty mainTex = FindProperty("_MainTex");
		editor.TexturePropertySingleLine(
			MakeLabel(mainTex, "Albedo (RGB)"), mainTex, FindProperty("_Color"));

		MaterialProperty intensitySlider = FindProperty("_Intensity");
		EditorGUI.indentLevel += 2;
		editor.ShaderProperty(intensitySlider, MakeLabel(intensitySlider));
		EditorGUI.indentLevel -= 2;

		MaterialProperty darkVisionSlider = FindProperty("_DarkVision");
		EditorGUI.indentLevel += 2;
		editor.ShaderProperty(darkVisionSlider, MakeLabel(darkVisionSlider));
		EditorGUI.indentLevel -= 2;
		

		editor.TextureScaleOffsetProperty(mainTex);

		MaterialProperty cloudTex = FindProperty("_CloudTex");
		editor.TexturePropertySingleLine(
			MakeLabel(cloudTex, "Cloud Texture"), cloudTex);

		MaterialProperty cutoffSlider = FindProperty("_CloudCutoff");
		EditorGUI.indentLevel += 2;
		editor.ShaderProperty(cutoffSlider, MakeLabel(cutoffSlider));
		EditorGUI.indentLevel -= 2;

		bool cloudColor = EditorGUILayout.Toggle(
			MakeLabel("Cloud Noise Color", "Have an interference cloud texture?"), 
			IsKeywordEnabled("_CLOUD_COLOR"));

		SetKeyword("_CLOUD_COLOR", cloudColor);

		bool cloudColorSplit = EditorGUILayout.Toggle(
			MakeLabel("Cloud Noise Split", "Should the cloud cutoff only appear on the splits?"), 
			IsKeywordEnabled("_CLOUD_COLOR_SPLIT"));

		SetKeyword("_CLOUD_COLOR_SPLIT", cloudColorSplit);

		MaterialProperty CloudSplitScaleAmount = FindProperty("_CloudSplitScale");
			editor.ShaderProperty(CloudSplitScaleAmount, 
			MakeLabel(CloudSplitScaleAmount, "Scaling for world pos"), 2);

		if(IsKeywordEnabled("_CLOUD_COLOR"))
		{
			MaterialProperty colorCloud = FindProperty("_CloudColor");
           	editor.ShaderProperty(colorCloud, "Color Cloud");
		}
		
		
		
	}

    void DoColorSplit()
    {
        MaterialProperty splitTex = FindProperty("_SplitTex");
        editor.TexturePropertySingleLine(
            MakeLabel(splitTex, "Split Tex"), splitTex);
        SetKeyword("_COLOR_SPLIT", splitTex.textureValue);
        if (IsKeywordEnabled("_COLOR_SPLIT"))
        {
			bool splitColorShare = EditorGUILayout.Toggle(
			MakeLabel("Split New Color", "Use different colors for the split?"), 
			IsKeywordEnabled("_COLOR_SPLIT_SHARE"));

			if(IsKeywordEnabled("_COLOR_SPLIT_SHARE"))
			{
           		MaterialProperty colorSplit = FindProperty("_ColorSplit");
           		editor.ShaderProperty(colorSplit, "Color Split");
			}

			MaterialProperty intensitySplitSlider = FindProperty("_IntensitySplit");
			EditorGUI.indentLevel += 2;
			editor.ShaderProperty(intensitySplitSlider, MakeLabel(intensitySplitSlider));
			EditorGUI.indentLevel -= 2;
			
			SetKeyword("_COLOR_SPLIT_SHARE", splitColorShare);
        }
    }

	void DoShiny () 
	{
		GUILayout.Label("Shiny Sheen", EditorStyles.boldLabel);

		MaterialProperty shinyTex =   FindProperty("_ShinyRamp");
		MaterialProperty shinyColor = FindProperty("_ShinyColor");
		MaterialProperty shinySpeed = FindProperty("_ShinySpeed");

		EditorGUI.BeginChangeCheck();

		editor.TexturePropertyWithHDRColor(
			MakeLabel(shinyTex, "Shiny Ramp"), shinyTex, shinyColor, // sheenConfig, (Pre-2018)
			false);
		editor.ShaderProperty(shinySpeed, MakeLabel(shinySpeed, "Shiny Speed"), 2);
		bool shineOffsetTex = EditorGUILayout.Toggle(
			MakeLabel("Shine Offset", "Have the shine amount be offset by the color of _Color?"), 
			IsKeywordEnabled("_SHINY_RAMP_OFFSET"));
			
		bool shineWorldPos = EditorGUILayout.Toggle(
			MakeLabel("Shine World", "Use world position instead of screen position?"), 
			IsKeywordEnabled("_SHINY_RAMP_WORLD_POS"));

		if(shineWorldPos)
		{			
			MaterialProperty shineWorldPosAmount = 	FindProperty("_ShinyPosSize");
			editor.ShaderProperty(shineWorldPosAmount, 
			MakeLabel(shineWorldPosAmount, "Scaling for world pos"), 2);
		}

		bool shineColorMain = EditorGUILayout.Toggle(
			MakeLabel("Shine _Color", "Use _Color in shine color calc?"), 
			IsKeywordEnabled("_SHINY_RAMP_COLOR_MULT"));
			
		
		if (EditorGUI.EndChangeCheck()) 
		{
			bool blank = false;
			float color = shinyColor.colorValue.a *
			(shinyColor.colorValue.r + shinyColor.colorValue.g + shinyColor.colorValue.b);
			if(color > 0.0f)
			{
				blank = true;
			}
			
			SetKeyword("_SHINY_RAMP_OFFSET", shineOffsetTex);
			SetKeyword("_SHINY_RAMP_WORLD_POS", shineWorldPos);
			SetKeyword("_SHINY_RAMP_COLOR_MULT", shineColorMain);
			SetKeyword("_SHINY_RAMP", blank && shinyTex.textureValue);
		}
	}
	
	void DoOffset()
	{
		MaterialProperty offsetAmount = FindProperty("_OffsetAmount");
		MaterialProperty offsetAngle = FindProperty("_OffsetAngle");
		Vector2 offset = new Vector2
		(
			offsetAngle.floatValue,
			offsetAmount.floatValue
		);

		Vector2 newOffset = EditorGUILayout.Vector2Field(MakeLabel("Z-Offset", "Y is the amount of Offset, with X influencing based on surface angle"), offset);
		
		if(newOffset != offset)
		{
			Debug.Log("New Offset");
			foreach (Material m in editor.targets) 
            {
				offsetAngle.floatValue = newOffset.x;
				offsetAmount.floatValue = newOffset.y;
			}
		}
	}

	void DoAlphaCutoff () 
    {
		MaterialProperty slider = FindProperty("_AlphaCutoff");
		EditorGUI.indentLevel += 2;
		editor.ShaderProperty(slider, MakeLabel(slider));
		EditorGUI.indentLevel -= 2;
	}

	MaterialProperty FindProperty (string name) 
    {
		return FindProperty(name, properties);
	}

	static GUIContent MakeLabel (string text, string tooltip = null) 
    {
		staticLabel.text = text;
		staticLabel.tooltip = tooltip;
		return staticLabel;
	}

	static GUIContent MakeLabel (
		MaterialProperty property, string tooltip = null) 
    {
		staticLabel.text = property.displayName;
		staticLabel.tooltip = tooltip;
		return staticLabel;
	}

	void SetKeyword (string keyword, bool state) 
    {
		if (state) {
			foreach (Material m in editor.targets) 
            {
				m.EnableKeyword(keyword);
			}
		}
		else {
			foreach (Material m in editor.targets) 
            {
				m.DisableKeyword(keyword);
			}
		}
	}

	bool IsKeywordEnabled (string keyword) 
    {
		return target.IsKeywordEnabled(keyword);
	}

	void RecordAction (string label) 
    {
		editor.RegisterPropertyChangeUndo(label);
	}
}