﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEngine.Rendering;
using UnityEditor;

public class WaterShaderGUI : ShaderGUI 
{
	enum RenderingMode 
    {
		Opaque, Fade
	}

	enum WaterType
	{
		Water, Shore, River, Estuary, Skeleton
	}

	struct RenderingSettings 
    {
		public RenderQueue queue;
		public string renderType;
		public BlendMode srcBlend, dstBlend;
		public bool zWrite;

		public static RenderingSettings[] modes = 
        {
			new RenderingSettings() 
            {
				queue = RenderQueue.Geometry+1,
				renderType = "Opaque",
				srcBlend = BlendMode.One,
				dstBlend = BlendMode.Zero,
				zWrite = true
			},
			new RenderingSettings() 
			{
				queue = RenderQueue.Transparent,
				renderType = "Transparent",
				srcBlend = BlendMode.SrcAlpha,
				dstBlend = BlendMode.OneMinusSrcAlpha,
				zWrite = true
			},
		};
	}

	static GUIContent staticLabel = new GUIContent();

	Material target;
	MaterialEditor editor;
	MaterialProperty[] properties;

	public override void OnGUI (MaterialEditor editor, MaterialProperty[] properties) 
    {
		this.target = editor.target as Material;
		this.editor = editor;
		this.properties = properties;
		DoWaterType();
		DoRenderingMode();
		DoMain();
		//DoNormals();
		GUILayout.Label("Secondary Options", EditorStyles.boldLabel);
		//DoDepth();
	}

	void DoWaterType()
	{
		WaterType mode = WaterType.Water;
		if (IsKeywordEnabled("_WATER_SHORE")) 
        {
			mode = WaterType.Shore;
		}
		else if (IsKeywordEnabled("_WATER_RIVER")) 
        {
			mode = WaterType.River;
		}
		else if (IsKeywordEnabled("_WATER_ESTUARY")) 
        {
			mode = WaterType.Estuary;
		}
		else if (IsKeywordEnabled("_WATER_SKELETON")) 
        {
			mode = WaterType.Skeleton;
		}

		EditorGUI.BeginChangeCheck();
		mode = (WaterType)EditorGUILayout.EnumPopup(
			MakeLabel("Water Type"), mode);		
		
		if (EditorGUI.EndChangeCheck()) 
        {
			RecordAction("Water Type");
			SetKeyword("_WATER_SHORE", mode == WaterType.Shore);
			SetKeyword("_WATER_RIVER", mode == WaterType.River);
		}
	}

	void DoRenderingMode () 
    {
		RenderingMode mode = RenderingMode.Opaque;
		if (IsKeywordEnabled("_RENDERING_FADE")) 
        {
			mode = RenderingMode.Fade;
		} 

		//MaterialProperty renderingOffset = FindProperty("_RenderQueueOffset");
		
		EditorGUI.BeginChangeCheck();
		mode = (RenderingMode)EditorGUILayout.EnumPopup(
			MakeLabel("Rendering Mode"), mode);
		//EditorGUILayout.IntField((int)renderingOffset.floatValue);		
		//editor.ShaderProperty(renderingOffset, MakeLabel(renderingOffset, "Rendering Offset"));

		if (EditorGUI.EndChangeCheck()) 
        {
			RecordAction("Rendering Mode");
			SetKeyword("_RENDERING_FADE", mode == RenderingMode.Fade);

			RenderingSettings settings = RenderingSettings.modes[(int)mode];
			foreach (Material m in editor.targets) 
            {
				//m.renderQueue = (int)settings.queue + (int)renderingOffset.floatValue;
				m.renderQueue = (int)settings.queue;
				m.SetOverrideTag("RenderType", settings.renderType);
				m.SetInt("_SrcBlend", (int)settings.srcBlend);
				m.SetInt("_DstBlend", (int)settings.dstBlend);
				m.SetInt("_ZWrite", settings.zWrite ? 1 : 0);
			}
		}
	}

	void DoMain () 
    {
		GUILayout.Label("Main Maps", EditorStyles.boldLabel);

		MaterialProperty mainTex = FindProperty("_MainTex");
		editor.TexturePropertySingleLine(
			MakeLabel(mainTex, "Albedo (RGB)"), mainTex, FindProperty("_Color"));

		editor.TextureScaleOffsetProperty(mainTex);


		DoScaling();
		DoMetallic();
		DoSmoothness(); 
	
		

		
	}

	void DoScaling()
	{
		MaterialProperty worldPosScale = FindProperty("_WorldPosScale");
		EditorGUI.indentLevel += 2;
		editor.ShaderProperty(worldPosScale, MakeLabel(worldPosScale));
		EditorGUI.indentLevel -= 2;
		
		if(IsKeywordEnabled("_WATER_SHORE"))
		{
			MaterialProperty worldPosScaleShore = FindProperty("_WorldPosScaleShore");
			EditorGUI.indentLevel += 2;
			editor.ShaderProperty(worldPosScaleShore, MakeLabel(worldPosScaleShore));
			EditorGUI.indentLevel -= 2;
		}
	}

	void DoMetallic () 
    {
		MaterialProperty slider = FindProperty("_Metallic");
		EditorGUI.indentLevel += 2;
		editor.ShaderProperty(slider, MakeLabel(slider));
		EditorGUI.indentLevel -= 2;
	}

	void DoSmoothness () 
    {
		MaterialProperty slider = FindProperty("_Smoothness");
		EditorGUI.indentLevel += 2;
		editor.ShaderProperty(slider, MakeLabel(slider));
		EditorGUI.indentLevel -= 2;
	}


	void DoNormals()
	{
		MaterialProperty map = FindProperty("_Bump1Tex");
		Texture tex = map.textureValue;
		EditorGUI.BeginChangeCheck();
		editor.TexturePropertySingleLine(
			MakeLabel(map), map,
			tex ? FindProperty("_Bump1Scale") : null);

		MaterialProperty Bump1Tex = FindProperty("_Bump1TexScale");
		editor.ShaderProperty(Bump1Tex, Bump1Tex.displayName);	
		
		//editor.TextureScaleOffsetProperty(map);
		if (EditorGUI.EndChangeCheck() && tex != map.textureValue) 
		{
			SetKeyword("_WATER_NORMAL_MAP", map.textureValue);
		}
		
		if (IsKeywordEnabled("_WATER_NORMAL_MAP")) 
        {
			map = FindProperty("_Bump2Tex");
			tex = map.textureValue;
			EditorGUI.BeginChangeCheck();
			editor.TexturePropertySingleLine(
				MakeLabel(map), map,
				tex ? FindProperty("_Bump2Scale") : null);					
			MaterialProperty Bump2Tex = FindProperty("_Bump2TexScale");
			editor.ShaderProperty(Bump2Tex, Bump2Tex.displayName);		

			//editor.TextureScaleOffsetProperty(map);
			if (EditorGUI.EndChangeCheck() && tex != map.textureValue) 
			{
				SetKeyword("_WATER_EXTRA_NORMAL_MAP", map.textureValue);
			}

		
			EditorGUI.indentLevel += 2;
			MaterialProperty timeSlider = FindProperty("_TimeSpeed");
			editor.ShaderProperty(timeSlider, MakeLabel(timeSlider));

			MaterialProperty foamSlider = FindProperty("_FoamAmount");
			editor.ShaderProperty(foamSlider, MakeLabel(foamSlider));

			MaterialProperty foamIntenseSlider = FindProperty("_FoamIntensity");
			editor.ShaderProperty(foamIntenseSlider, MakeLabel(foamIntenseSlider));
			EditorGUI.indentLevel -= 2;
			
		}
	}

	void DoDepth()
	{
		EditorGUI.BeginChangeCheck();
		
		MaterialProperty depthSlider = FindProperty("_WaterDepth");

		MaterialProperty depthRamp = FindProperty("_WaterDepthRamp");
		editor.TexturePropertySingleLine(
		MakeLabel(depthRamp), depthRamp, depthSlider);

		editor.TextureScaleOffsetProperty(depthRamp);

		if (EditorGUI.EndChangeCheck()) 
		{			
			SetKeyword("_WATER_DEPTH", depthSlider.floatValue < 1.0f);
		}
	}

	MaterialProperty FindProperty (string name) 
    {
		return FindProperty(name, properties);
	}

	static GUIContent MakeLabel (string text, string tooltip = null) 
    {
		staticLabel.text = text;
		staticLabel.tooltip = tooltip;
		return staticLabel;
	}

	static GUIContent MakeLabel (
		MaterialProperty property, string tooltip = null) 
    {
		staticLabel.text = property.displayName;
		staticLabel.tooltip = tooltip;
		return staticLabel;
	}

	void SetKeyword (string keyword, bool state) 
    {
		if (state) {
			foreach (Material m in editor.targets) 
            {
				m.EnableKeyword(keyword);
			}
		}
		else {
			foreach (Material m in editor.targets) 
            {
				m.DisableKeyword(keyword);
			}
		}
	}

	bool IsKeywordEnabled (string keyword) 
    {
		return target.IsKeywordEnabled(keyword);
	}

	void RecordAction (string label) 
    {
		editor.RegisterPropertyChangeUndo(label);
	}

}


#endif